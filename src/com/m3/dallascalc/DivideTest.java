package com.m3.dallascalc;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

class DivideTest {
	
	Divide result;
	
	@BeforeEach
	void setup() {
		result = new Divide();
	}
	
	@Test
	void verifyNotNull() {
		String msg = "Could not generate object of class under test.";
		assertNotNull(msg, result);
	}
	
	@Test
	void divide_by_0_test() {
		Double dividend = 1.0;
		Double divisor = 0.0;
		
		String msg = "Did not reject divide by 0.";
		
		Executable closure = () -> result.evaluate(dividend, divisor);
		assertThrows(ArithmeticException.class, closure, msg);
	}
	
	@Test
	void simple_divide_test() {
		
		Double dividend = 4.0;
		Double divisor = 2.0;
		Double expected = 2.0;
		Double actual = result.evaluate(dividend, divisor);
		Double tolerance = 0.00000002;
		
		String msg = "Did not divide correctly.";
		
		assertEquals(msg, expected, actual, tolerance);
		
	}
	
	@Test
	void divisor_larger_test(){
		Double dividend = 2.0;
		Double divisor = 4.0;
		Double expected = 0.5;
		Double actual = result.evaluate(dividend, divisor);
		Double tolerance = 0.00000002;
		
		String msg = "Did not divide when divisor is larger correctly.";
		
		assertEquals(msg, expected, actual, tolerance);
	}
	
	@Test
	void dividend_equals_divisor_test() {
		Double dividend = 4.0;
		Double divisor = 4.0;
		Double expected = 1.0;
		Double actual = result.evaluate(dividend, divisor);
		Double tolerance = 0.00000002;
		
		String msg = "Did not divide when divisor and dividend are equal correctly.";
		assertEquals(msg, expected, actual, tolerance);
	}


}
