package com.m3.dallascalc;

import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.function.Executable;

<<<<<<< HEAD
=======
import com.m3.dallascalc.Multiply;

>>>>>>> 01868e9ff54956302ad0b5320528a3044d5e293c
public class MultiplyTest {

	iEvaluate objectundertest;
	
	@BeforeEach
	void setup() {
		objectundertest = new Multiply();
	}
	
	void testEvaluateOverflow() {
		Double input1;
		Double input2;
		Double result = input1*input2;
		if(result > Multiply.DOUBLE_LIMIT) {
			String msg = "Final result is too large for boundaries.";
			Executable closure = () -> objectUnderTest.evaluate(input1, input2); 
			assertThrows(IllegalArgumentException.class, closure, msg); 
		}
	}
}
