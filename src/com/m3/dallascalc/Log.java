package com.m3.dallascalc;

public class Log implements iEvaluate {

	@Override
	public Double evaluate(Double num, Double base) {
		if(base == 0 || base == 1) {
			throw new ArithmeticException();
		}
		return Math.log(num)/Math.log(base);
	}
	
	public Double evaluate(Double num) {
		return Math.log10(num);
	}

}
