package com.m3.dallascalc;

public class Multiply implements iEvaluate{
	static final Double DOUBLE_LIMIT = 1.7*(10^308);
	Double number1;
	Double number2;
	Double result;
	
	public Double evaluate(Double number1, Double number2) {
		result = number1 * number2;
		return result;
	}

}
