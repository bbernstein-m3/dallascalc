package com.m3.dallascalc;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.Assert.assertEquals;

public class LogTest {
	
	Log testLogObject;
	
	@BeforeEach
	void setup() {
		testLogObject = new Log();
	}
	
	@Test
	void test_log() {
		String msg = "Did not calculate logarithm. Data: 9.0, 3.0";
		Double input = 9.0;
		Double base = 3.0;
		Double expected = 2.0;
		Double actual;
		Double tolerance = 0.00001;
		actual = testLogObject.evaluate(input, base);
		assertEquals(msg, expected, actual, tolerance);
	}

	@Test
	void test_log_negativeOutput() {
		String msg = "Did not calculate logarithm. Data: 0.0625, 2.0";
		Double input = 0.0625;
		Double base = 2.0;
		Double expected = -4.0;
		Double actual;
		Double tolerance = 0.00001;
		actual = testLogObject.evaluate(input, base);
		assertEquals(msg, expected, actual, tolerance);
	}

	@Test
	void test_log_autoBase10() {
		String msg = "Did not calculate base-10 logarithm. Data: 1000";
		Double input = 1000.00;
		Double expected = 3.0;
		Double actual;
		Double tolerance = 0.00001;
		actual = testLogObject.evaluate(input);
		assertEquals(msg, expected, actual, tolerance);
	}
}
