package com.m3.dallascalc;

public class Divide implements iEvaluate {

	@Override
	public Double evaluate(Double dividend, Double divisor) {
		if (divisor == 0) {
			throw new ArithmeticException();
		}
		
		return dividend/divisor;
	}
	
}
