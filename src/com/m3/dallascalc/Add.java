package com.m3.dallascalc;

public class Add implements iEvaluate
{
  public Double evaluate(Double operator1, Double operator2)
  {
    Double solution = operator1 + operator2;
    if(solution >= Double.MAX_VALUE)
    {
      String msg = "The addition of the two numbers is too large.";
      throw new ArithmeticException(msg);
    }
    if(solution <= Double.MIN_VALUE)
    {
      String msg = "The addition of the too numbers creates a number that is too small.";
      throw new ArithmeticException(msg);
    }
    return solution;
  }
}
