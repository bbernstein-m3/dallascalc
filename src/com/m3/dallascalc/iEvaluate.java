package com.m3.dallascalc;

public interface iEvaluate {

	public Double evaluate(Double input1, Double input2);

}
