import com.m3.dallascalc.iEvaluate;

public class Subtract implements iEvaluate {
	
	public Double evaluate(Double operator1, Double operator2) {
		Double solution = operator1 - operator2;
		if (solution >= Double.MAX_VALUE) {
			String msg = "The subtraction of these two numbers is too high";
			throw new ArithmeticException(msg);
		}
		
		if (solution <= Double.MIN_VALUE) {
			String msg = "The subtraction of these two numbers is too low";
			throw new ArithmeticException(msg);
		}
		
		return solution;
	}

}
