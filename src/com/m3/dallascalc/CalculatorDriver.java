import java.util.Scanner;

public class CalculatorDriver {

	Scanner reader = new Scanner(System.in);
	Double total;
	
	public CalculatorDriver() {
		
	
	String msg = getUserInput();
	System.out.println();

	}
	
	public String getUserInput() {
		String userInput;
		System.out.println("Please type in the one you are calculating.");
		userInput = reader.nextLine();
		if(splitInput(userInput)) {
			
		}
		else {
			System.out.println("Something went wrong");
		}


		return userInput; 
	}
	
	public boolean splitInput(String userInput) {
		boolean isSuccess = false;
		
		String[] result = userInput.split("(?<=[-+*/])|(?=[-+*/])");
		Double value1 = Double.parseDouble(result[0]);
		Double value2 = Double.parseDouble(result[2]);
		
		 switch(result[1]) {
		 case "+":
			 	Add addNumbers = new Add();
			 	 this.total = addNumbers.evaluate(value1, value2);
				break;	
		 case "-":
			 Subtract subtractNumbers;
			 	this.total = subtractNumbers.evaluate(value1,  value2);
			 	System.out.println("minus");
			 	break;
		 case "*":
		 case "x":
			 Multiply multiplyNumbers;
			 this.total = multiplyNumbers.evaluate(value1, value2);
			 System.out.println("Multiplying");
			 break;
		 case "/":
			 Divide divideNumbers;
			 this.total = divideNumbers.evaluate(value1, value2);
			 System.out.println("Divide");
			 break;
		
		
		 default:
			System.out.println("something went wrong");
		 }
		
	}
}
