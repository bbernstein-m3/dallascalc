package com.m3.dallascalc;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

class TestAdd {
	iEvaluate objectUnderTest;
	
	@BeforeEach
	void setup() {
		objectUnderTest = new Add();
	}

	@Test
	void test_evaluate_add() {
		String msg = "Did not add successfully.";
		Double input1 = -1.0;
		Double input2 = 159.0;
		Double actual;
		Double expected = 158.0;
		Double tolerance = 0.00000002;
		actual = objectUnderTest.evaluate(input1, input2);
	    assertEquals(msg, actual, expected, tolerance);
	}
}