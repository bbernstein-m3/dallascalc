
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.assertThat;

import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;

import org.junit
import com.mthree.training.junitDemo.Caculator;

public class TestSubtraction {
	Subtract subtractionTestUnit;
	@BeforeEach
	void initalize() 
	{
	subtractionTestUnit=new Subtract();
	}
	@Test
	public void testReturnsADouble() {
		assertThat("Value is not double",Subtract.testAddAndGemmieDouble(1,2),instanceOf(Double.class));
	}
	@Test
	public void test1minus1() {
		assertEquals("1-1 is not 0",subtractionTestUnit.doSubtract(1.0,1),0);
	}
	@Test
	public void test49minus100andaHalf() {
		assertEquals("49-100 is not -51.5",subtractionTestUnit.doSubtract(49,100.5),-51.5);
	}
	@Test
	public void testUnderFlow(){
		//Executable closureContainingCodeToTest = () -> throw new IllegalArgumentException("a message");

	    assertThrows(Arith, closureContainingCodeToTest, "a message");
	}
	@Test
	public void testOverFlow() {
		fail("Not yet implemented");
	}
}
